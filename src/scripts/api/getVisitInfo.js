import VisitCardiologist from "../classes/VisitCardiologist.js";
import VisitDentist from "../classes/VisitDentist.js";
import VisitTherapist from "../classes/VisitTherapist.js";

const getVisitInfo = async (token) => {
  const res = await fetch("https://ajax.test-danit.com/api/v2/cards/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

  document.querySelector(".cards-box").innerHTML = "";
  res.forEach((el) => {
    if (el.urgency == "ordinary") {
      el.urgency = "обычная";
    } else if (el.urgency == "priority") {
      el.urgency = "приоритетная";
    } else if (el.urgency == "urgent") {
      el.urgency = "неотложная";
    }

    if (el.doctor == "dentist") {
      el.doctor = "Стоматолог";
      const {
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        moreInfo: { lastVisit },
      } = el;
      const visitDentist = new VisitDentist(
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        lastVisit
      );
      document
        .querySelector(".cards-box")
        .insertAdjacentElement("beforeend", visitDentist.createFullCard());
    } else if (el.doctor == "cardiologist") {
      el.doctor = "Кардиолог";
      const {
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        moreInfo: { pressure, bms, pastIllnesses, age },
      } = el;
      const visitCardiologist = new VisitCardiologist(
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        pressure,
        bms,
        pastIllnesses,
        age
      );
      document
        .querySelector(".cards-box")
        .insertAdjacentElement("beforeend", visitCardiologist.createFullCard());
    } else if (el.doctor == "therapist") {
      el.doctor = "Терапевт";
      const {
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        moreInfo: { age },
      } = el;
      const visitTherapist = new VisitTherapist(
        id,
        name,
        doctor,
        visitTarget,
        urgency,
        desc,
        age
      );
      document
        .querySelector(".cards-box")
        .insertAdjacentElement("beforeend", visitTherapist.createFullCard());
    }
  });
  return;
};

export default getVisitInfo;
