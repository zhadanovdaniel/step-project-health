import loginModal from "./loginModal.js";
import cardCreate from "./cardCreate.js";
import getVisitInfo from "./api/getVisitInfo.js";
import getGilteredInputValue from "./filter/inputFilter.js";
import getFilteredPriority from "./filter/priorityFilter.js";

(() => {
  if (localStorage.getItem("token")) {
    const token = localStorage.getItem("token");
    document.querySelector(
      ".navbar__login"
    ).textContent = `Выйти из ${localStorage.email}`;
    getVisitInfo(token);
  }
})();

const filterBtn = document.querySelector(".filter__open-btn");
const filterIcon = document.querySelector(".filter__open-icon");
const filterForm = document.querySelector(".filter__form");

filterBtn.addEventListener("click", () => {
  filterIcon.classList.toggle("open");
  filterForm.classList.toggle("open");
});

const loginBtn = document.querySelector(".navbar__login_link");
loginBtn.addEventListener("click", () => {
  if (document.querySelector(".navbar__login").textContent !== "Войти") {
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    document.querySelector(".navbar__login").textContent = "Войти";
    document.querySelector(".cards-box").innerHTML = "";
  } else {
    new loginModal().render(".main-page");
  }
});

const createVisitBtn = document.querySelector(".navbar__create-visit");
createVisitBtn.addEventListener("click", () => {
  new cardCreate().render(".main-page");
});

const filterInput = document.querySelector(".filter__search");
const filterSelect = document.querySelector(".filter__priority");
filterInput.addEventListener("input", () => {
  document.querySelectorAll(".card").forEach((el) => {
    el.hidden = false;
  });
  getFilteredPriority(filterSelect);
  getGilteredInputValue(filterInput);
});

filterSelect.addEventListener("change", () => {
  document.querySelectorAll(".card").forEach((el) => {
    el.hidden = false;
  });
  getFilteredPriority(filterSelect);
});
