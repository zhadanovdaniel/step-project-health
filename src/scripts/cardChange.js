import getVisitInfo from "./api/getVisitInfo.js";
import postCardCardiolist from "./postCardCardiolist.js";
import postCardDentist from "./postCardDentist.js";
import postCardTherapist from "./postCardTherapist.js";

const cardChange = (selector, cardID, name, visitTarget, desc, urgency, doctor, pressure, bms, pastIllnesses, age, lastVisit) => {
  const urgencyType = ["", "", ""];
  const doctorType = ["", "", ""];
  if (urgency == "обычная") urgencyType[0] = `selected = "selected"`
  else if (urgency == "приоритетная") urgencyType[1] = `selected = "selected"`
  else if (urgency == "неотложная") urgencyType[2] = `selected = "selected"`

  if (doctor == "Кардиолог") doctorType[0] = `selected = "selected"`
  else if (doctor == "Стоматолог") doctorType[1] = `selected = "selected"`
  else if (doctor == "Терапевт") doctorType[2] = `selected = "selected"`

  document.querySelector(".body-page").classList.add("blur");
  document.querySelector(selector).insertAdjacentHTML(
    "beforeend",
    `<section class="cards-modal">
<h2 class="cards-modal__title">Изменить карточку</h2>
<form action="" method="post" class="cards-modal__form">
  <div class="cards-modal__content">
    <div class="cards-modal__section">
      <label for="fullname" class="cards-modal__fullname_label"
        >ФИО:</label
      >
      <input
        required
        type="text"
        class="cards-modal__fullname"
        id="fullname"
        value="${name}"
      />
      <label for="target" class="cards-modal__target_label"
        >Цель визита:</label
      >
      <input
        required
        type="text"
        class="cards-modal__target"
        id="target"
        value="${visitTarget}"
      />
      <label for="desc" class="cards-modal__desc_label"
        >Краткое описание визита:</label
      >
      <input required type="text" class="cards-modal__desc" id="desc" value="${desc}"/>
      <p >Приоритетность:</p>
      <select
        name="priority"
        id="priority"
        class="cards-modal__priority"
      >
        <option ${urgencyType[0]} value="ordinary">Обычная</option>
        <option ${urgencyType[1]} value="priority">Приоритетная</option>
        <option ${urgencyType[2]} value="urgent">Неотложная</option>
      </select>
      <select name="doctors" id="priority" class="cards-modal__doctors" disabled>
        <option ${doctorType[0]} value="cardiologist">Кардиолог</option>
        <option ${doctorType[1]} value="dentist">Стоматолог</option>
        <option ${doctorType[2]} value="therapist">Терапевт</option>
      </select>
    </div>
    <div class="cards-modal__section">
      <label for="pressure" class="cards-modal__pressure_label"
        >Обычное давление:</label
      >
      <input
        required
        type="text"
        class="cards-modal__pressure"
        id="pressure"
      />
      <label for="weight-index" class="cards-modal__weight-index_label"
        >Индекс массы тела:</label
      >
      <input
        required
        type="text"
        class="cards-modal__weight-index"
        id="weight-index"
      />
      <label for="diseases" class="cards-modal__diseases_label"
        >Перенесенные заболевания сердечно-сосудистой системы:</label
      >
      <input
        required
        type="text"
        class="cards-modal__diseases"
        id="diseases"
      />
      <label for="age" class="cards-modal__age_label">Возвраст:</label>
      <input required type="text" class="cards-modal__age" id="age" />
    </div>
  </div>
  <button class="cards-modal__btn cards-modal__btn_cancel">
    Отменить
  </button>
  <button class="cards-modal__btn cards-modal__btn_submit" type="submit">
    Изменить
  </button>
</form>
</section>`
  );




  document.querySelectorAll(".cards-modal__section")[1].innerHTML = "";


  if (document.querySelector(".cards-modal__doctors").value === "cardiologist") {
    document
      .querySelectorAll(".cards-modal__section")[1]
      .insertAdjacentHTML(
        "beforeend",
        `<label for="pressure" class="cards-modal__pressure_label"
        >Обычное давление:</label
      >
      <input
        required
        type="text"
        class="cards-modal__pressure"
        id="pressure"
        value="${pressure}"
      />
      <label for="weight-index" class="cards-modal__weight-index_label"
        >Индекс массы тела:</label
      >
      <input
        required
        type="text"
        class="cards-modal__weight-index"
        id="weight-index"
        value="${bms}"
      />
      <label for="diseases" class="cards-modal__diseases_label"
        >Перенесенные заболевания сердечно-сосудистой системы:</label
      >
      <input
        required
        type="text"
        class="cards-modal__diseases"
        id="diseases"
        value="${pastIllnesses}"
      />
      <label for="age" class="cards-modal__age_label">Возвраст:</label>
      <input required type="text" class="cards-modal__age" id="age" value="${age}"/>`
      );
  } else if (document.querySelector(".cards-modal__doctors").value === "dentist") {
    document
      .querySelectorAll(".cards-modal__section")[1]
      .insertAdjacentHTML(
        "beforeend",
        `<label for="last-visit" class="cards-modal__last-visit_label">Дата последнего посещения:</label>
        <input required type="text" class="cards-modal__last-visit" id="last-visit" value="${lastVisit}"/>`
      );
  } else {
    document
      .querySelectorAll(".cards-modal__section")[1]
      .insertAdjacentHTML(
        "beforeend",
        `<label for="age" class="cards-modal__age_label">Возвраст:</label>
          <input required type="text" class="cards-modal__age" id="age" value="${age}"/>`
      );
  }


  document
    .querySelector(".cards-modal__form")
    .addEventListener("submit", (e) => {
      e.preventDefault();

      const form = document.querySelector(".cards-modal__form");
      const formArr = Array.from(form).map((el) => el.value);
      const token = localStorage.getItem("token");

      if (formArr[4] === "cardiologist") {
        const [
          name,
          visitTarget,
          desc,
          urgency,
          doctor,
          pressure,
          bms,
          passIllnesses,
          age,
        ] = formArr;
        postCardCardiolist(
          token,
          name,
          doctor,
          visitTarget,
          urgency,
          desc,
          pressure,
          bms,
          passIllnesses,
          age,
          "PUT",
          cardID
        )
        getVisitInfo(token);

        document.querySelector(".body-page").classList.remove("blur");
        document.querySelector(".cards-modal").remove();
      } else if (formArr[4] === "dentist") {
        const [name, visitTarget, desc, urgency, doctor, lastVisit] = formArr;
        postCardDentist(
          token,
          name,
          doctor,
          visitTarget,
          urgency,
          desc,
          lastVisit,
          "PUT",
          cardID
        )
        getVisitInfo(token);
        document.querySelector(".body-page").classList.remove("blur");
        document.querySelector(".cards-modal").remove();
      } else if (formArr[4] === "therapist") {
        const [name, visitTarget, desc, urgency, doctor, age] = formArr;
        postCardTherapist(
          token,
          name,
          doctor,
          visitTarget,
          urgency,
          desc,
          age,
          "PUT",
          cardID
        )
        getVisitInfo(token);
        document.querySelector(".body-page").classList.remove("blur");
        document.querySelector(".cards-modal").remove();
      }
      getVisitInfo(token)
    });

  document
    .querySelector(".cards-modal__btn_cancel")
    .addEventListener("click", () => {
      document.querySelector(".body-page").classList.remove("blur");
      document.querySelector(".cards-modal").remove();
    });
}


export default cardChange;