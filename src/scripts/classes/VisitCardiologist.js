import Visit from "./visit.js";

class VisitCardiologist extends Visit {
   constructor(cardID, name, doctor, vistTarget, urgency, desc, pressure, bms, pastIllnesses, age) {
      super(cardID, name, doctor, vistTarget, urgency, desc)
      this.pressure = pressure;
      this.bms = bms;
      this.pastIllnesses = pastIllnesses;
      this.age = age;
   }

   createFullCard() {
      const card = this.createCard()

      card.querySelector(".show-more-args").innerHTML = `
      <p class="card-atribute"><span>Обычное давление: </span>${this.pressure}</p>
      <p class="card-atribute"><span>Индекс массы тела: </span>${this.bms}</p>
      <p class="card-atribute"><span>Перенесенные заболевания сердечно-сосудистой системы: </span>${this.pastIllnesses}</p>
      <p class="card-atribute"><span>Возраст: </span>${this.age}</p>
      `


      return card
   }
}

export default VisitCardiologist;