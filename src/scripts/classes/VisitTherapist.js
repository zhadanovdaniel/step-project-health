import Visit from "./visit.js";

class VisitTherapist extends Visit {
   constructor(cardID, name, doctor, vistTarget, urgency, desc, age) {
      super(cardID, name, doctor, vistTarget, urgency, desc)
      this.age = age
   }

   createFullCard() {
      const card = this.createCard()

      card.querySelector(".show-more-args").innerHTML = `<p class="card-atribute"><span>Возраст: </span>${this.age}</p>`


      return card
   }
}

export default VisitTherapist;